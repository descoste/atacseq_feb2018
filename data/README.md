The `data` dir should contain all the data used as input of the code :

* `data/sequencing` : contain all sequencing data distributed by format in sub-folders i.e. `bam`, `fastq`, ...
* `data/genome` : in here place all the genome associated data like fasta, GFF, indices etc... 
                  You will most likely link a lot of these files from external directories (the datawarehouse, the genomes folder ...)
* third party data like data from the warehouse or published data
* create other folders as you see fit and do not forget to declare these dir in the `config/config.yml`

