The analysis performed in this repository were used in:

Serina Secanechia, Y.N., Bergiers, I., Rogon, M. et al. Identifying a novel role for the master regulator Tal1 in the Endothelial to Hematopoietic Transition. Sci Rep 12, 16974 (2022). https://doi.org/10.1038/s41598-022-20906-0
