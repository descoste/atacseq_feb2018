#!/bin/bash

#SBATCH --array=1-7
#SBATCH --nodes 1
#SBATCH --mem=1gb
#SBATCH --job-name explorediffTFresults
#SBATCH --ntasks=1
#SBATCH --time 01:00:00
#SBATCH --output slurm_%x_%A_%a.out


srun perl ../perl/explorediffTFresults.pl $SLURM_ARRAY_TASK_ID $1
