# Modify this Snakemake call to your needs.
# If you run the analysis on a cluster, we recommend using a cluster configuration via --cluster-config

echo "####################################################################"
echo "#              Thank you for the interest in diffTF!               #"
echo "#    If you have questions or comments, feel free to contact us.   #"
echo "# We will be happy to answer any questions related to this project #"
echo "#    as well as questions related to the software implementation.  #"
echo "####################################################################"


echo "\nThis wrapper script executes Snakemake to start diffTF. Modify the Snakemake call to your needs.\n"

echo "#########################################################"
echo "#   NOTE THAT THIS ANALYSIS MAY TAKE A WHILE TO FINISH  #"
echo "# INCREASING THE NUMBER OF CORES SPEEDS UP THE ANALYSIS #"
echo "#########################################################"

 

# Real run, using 16 cores, use conda env "source activate $LANCRINENV"

#/g/funcgen/bin/snakemake --snakefile /scratch/descostes/lancrin/software/diffTF/src/Snakefile --configfile ../../../diffTF/20190102_i5untvsDoxEndo/config.json --latency-wait 30 --notemp --rerun-incomplete --reason --keep-going --cores 16 --local-cores 1 --jobs 400 --cluster-config ../../../diffTF/cluster.largeAnalysis.json --cluster " sbatch -p {cluster.queue} -J {cluster.name} --cpus-per-task {cluster.nCPUs} --mem {cluster.memory} --time {cluster.maxTime} -o \"{cluster.output}\" -e \"{cluster.error}\"  --mail-type=None --parsable " --use-singularity --singularity-args " --bind /g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/ /scratch/descostes" --singularity-prefix ../../../../config/diffTF_singularity 


snakemake --snakefile /scratch/descostes/lancrin/software/diffTF/src/Snakefile --configfile ../../../diffTF/20190102_i5untvsDoxEndo/config.json --latency-wait 60 --notemp --rerun-incomplete --reason --keep-going --cores 16 --local-cores 1 --jobs 400 --cluster-config ../../../diffTF/cluster.largeAnalysis.json --cluster " sbatch -p {cluster.queue} -J {cluster.name} --cpus-per-task {cluster.nCPUs} --mem {cluster.memory} --time {cluster.maxTime} -o \"{cluster.output}\" -e \"{cluster.error}\"  --mail-type=None --parsable " --use-singularity --singularity-args "--bind /g/lancrin/,/scratch/descostes" --singularity-prefix ../../../../config/diffTF_singularity
 
#snakemake --snakefile /scratch/descostes/lancrin/software/diffTF/src/Snakefile --configfile ../../../diffTF/20190102_i5untvsDoxEndo/config.json --latency-wait 30 --notemp --rerun-incomplete --reason --keep-going --cores 16  --use-singularity --singularity-args "--bind /g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/,/scratch/descostes" --singularity-prefix ../../../../config/diffTF_singularity

#/g/funcgen/bin/snakemake --snakefile /scratch/descostes/lancrin/software/diffTF/src/Snakefile --configfile ../../../diffTF/20190102_i5untvsDoxEndo/config.json --latency-wait 30 --notemp --rerun-incomplete --reason --keep-going --cores 16  --use-singularity --singularity-args "--bind /g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/,/scratch/descostes,/g/funcgen/" --singularity-prefix ../../../../config/diffTF_singularity