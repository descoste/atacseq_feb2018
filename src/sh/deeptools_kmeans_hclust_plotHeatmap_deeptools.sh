#!/bin/bash

#SBATCH --array=1-10
#SBATCH --nodes 1
#SBATCH --cpus-per-task=10
#SBATCH --mem=20gb
#SBATCH --job-name DeeptoolsClusteringHclust
#SBATCH --ntasks=1
#SBATCH --time 06:00:00
#SBATCH --output slurm_%x_%A_%a.out

module load deeptools/3.1.3-foss-2018b-Python-3.7.0

srun perl ../perl/deeptools_kmeans_hclust_plotHeatmap.pl $SLURM_ARRAY_TASK_ID $1
