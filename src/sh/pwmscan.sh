#!/usr/bin/bash


ALLFILES=(`ls /g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/pwmscan/hocomoco_v11_mm10/pwm_integer/*`)


for currentFile in "${ALLFILES[@]}"
do
	current_name=`basename ${currentFile}`
	echo $current_name
	echo "#!/bin/bash
		#SBATCH --nodes 1
		#SBATCH --cpus-per-task=4
		#SBATCH --mem=1gb
		#SBATCH --job-name pwmscan
		#SBATCH --ntasks=1
		#SBATCH --time 0:10:00
		#SBATCH --output slurm_%x_%A_%a.out
		
		srun singularity exec --bind /g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/:/mount /g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/singularity/pwmscan.sif /home/local/bin/pwm_scan -m /mount/analysis/pwmscan/hocomoco_v11_mm10/pwm_integer/$current_name -e 0.00001 -d /mount/softwares/pwmscan/genomedb -s mm10 -w -p 4" > job_file.sh
	sbatch job_file.sh
done
	




