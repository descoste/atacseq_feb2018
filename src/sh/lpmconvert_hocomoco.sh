#!/usr/bin/bash

# This script is run in local and aims at converting pwm matrices of hocomocov11
# to lpm matrices. They then are converted to integer with lpmconvert_hocomocov11.sh
# This script is run in local because of pb with perl modules installations.
# Descostes feb 2020

ALLFILES=(`ls /home/descostes/Documents/analysis/lancrin/convert_matrices/lpm/*`)

for current_file in "${ALLFILES[@]}"
	do
		current_folder=`dirname ${current_file}`
		output_folder=$(echo $current_folder | sed 's/lpm/pwm_integer/')
		current_name=`basename ${current_file}`
		output_file=$(echo $current_name | sed 's/.lpm/-int.pwm/')
		echo "	### Output: $output_folder/$output_file"
		perl ../perl/lpmconvert.pl -o $output_folder/$output_file $current_file
	done
