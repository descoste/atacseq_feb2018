#!/bin/bash

#SBATCH --array=1-1
#SBATCH --nodes 1
#SBATCH --cpus-per-task=10
#SBATCH --mem=15gb
#SBATCH --job-name MatrixDeeptools
#SBATCH --ntasks=1
#SBATCH --time 08:00:00
#SBATCH --output slurm_%x_%A_%a.out

module load deeptools/3.1.3-foss-2018b-Python-3.7.0

srun perl ../perl/deeptools_computematrix.pl $SLURM_ARRAY_TASK_ID $SLURM_CPUS_ON_NODE $1
