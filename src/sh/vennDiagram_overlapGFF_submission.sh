#!/bin/bash

#SBATCH --array=1-7
#SBATCH --nodes 1
#SBATCH --mem=4gb
#SBATCH --job-name Venn
#SBATCH --ntasks=1
#SBATCH --time 00:30:00
#SBATCH --output slurm_%x_%A_%a.out


module load R/3.5.0-foss-2017b-X11-20171023

srun perl ../perl/vennDiagram_overlapGFF.pl $SLURM_ARRAY_TASK_ID $1
