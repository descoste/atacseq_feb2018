#!/bin/bash

#SBATCH --array=1-210
#SBATCH --nodes 1
#SBATCH --mem=1gb
#SBATCH --job-name macs2
#SBATCH --ntasks=1
#SBATCH --time 00:02:00
#SBATCH --output slurm_%x_%A_%a.out

module load R/3.5.0-foss-2017b-X11-20171023

srun perl ../perl/peaks_to_gff.pl $SLURM_ARRAY_TASK_ID $1
