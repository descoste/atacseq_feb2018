#!/bin/bash

#SBATCH --array=1-210
#SBATCH --nodes 1
#SBATCH --mem=1gb
#SBATCH --job-name macs2
#SBATCH --ntasks=1
#SBATCH --time 00:30:00
#SBATCH --output slurm_%x_%A_%a.out

module load MACS2/2.1.0.20150731-foss-2016b-Python-2.7.12

srun perl ../perl/macs2.pl $SLURM_ARRAY_TASK_ID $1
