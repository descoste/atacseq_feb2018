#!/bin/bash

#SBATCH --array=1-11
#SBATCH --nodes 1
#SBATCH --mem=5gb
#SBATCH --job-name diffbind
#SBATCH --ntasks=1
#SBATCH --time 02:50:00
#SBATCH --cpus-per-task=10
#SBATCH --output slurm_%x_%A_%a.out

module load R/3.5.0-foss-2017b-X11-20171023

srun perl ../perl/diffbind.pl $SLURM_ARRAY_TASK_ID $1
