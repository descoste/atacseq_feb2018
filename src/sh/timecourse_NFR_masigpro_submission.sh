#!/bin/bash

#SBATCH --array=1-1
#SBATCH --nodes 1
#SBATCH --mem=15gb
#SBATCH --job-name masigpro
#SBATCH --ntasks=1
#SBATCH --time 02:00:00
#SBATCH --output slurm_%x_%A_%a.out

module load R/3.5.0-foss-2017b-X11-20171023

srun perl ../perl/timecourse_NFR_masigpro.pl $SLURM_ARRAY_TASK_ID $1
