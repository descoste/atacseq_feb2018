#!/bin/bash

#SBATCH --array=1-1
#SBATCH --nodes 1
#SBATCH --cpus-per-task=5
#SBATCH --mem=1gb
#SBATCH --job-name Union
#SBATCH --ntasks=1
#SBATCH --time 00:00:01

module load R/3.5.0-foss-2017b-X11-20171023

srun perl ../perl/union_NFR.pl $SLURM_ARRAY_TASK_ID $SLURM_CPUS_ON_NODE $1
