#!/bin/bash

#SBATCH --array=1-30
#SBATCH --nodes 1
#SBATCH --cpus-per-task=5
#SBATCH --mem=1gb
#SBATCH --job-name Elongation
#SBATCH --ntasks=1
#SBATCH --time 00:15:00

module load deeptools/3.1.3-foss-2018b-Python-3.7.0

srun perl ../perl/bamPEFragmentSize.pl $SLURM_ARRAY_TASK_ID $SLURM_CPUS_ON_NODE $1
