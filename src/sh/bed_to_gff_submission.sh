#!/bin/bash

#SBATCH --array=1-3
#SBATCH --nodes 1
#SBATCH --mem=2000
#SBATCH --job-name bed2gff
#SBATCH --ntasks=1
#SBATCH --time 00:01:00
#SBATCH --output slurm_%x_%A_%a.out


srun perl ../perl/bed_to_gff.pl $SLURM_ARRAY_TASK_ID $1
