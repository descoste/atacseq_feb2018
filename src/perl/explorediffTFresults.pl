###############
# Descostes March 2019
###############

use strict;
my $file_lineNumber = $ARGV[0];
my $input_file = $ARGV[1];


#Retrieve the parameters
my $file_path = `head -n $file_lineNumber $input_file | tail -n1`;
chomp $file_path;

my @arguments_tab = split(';', $file_path);

if(scalar(@arguments_tab) != 3)
{
    die("Missing arguments for explorediffTFresults.pl\n\n File should contain: see script\n\n");
}

my $diffTF_results_vec = $arguments_tab[0];
my $output_folder_vec = $arguments_tab[1];
my $genes_annotations_file = $arguments_tab[2];

print "This is job number $file_lineNumber \n";

print "Rscript ../R/explorediffTFresults.R --diffTFResultsVec $diffTF_results_vec --outputFolderVec $output_folder_vec --genesAnnotationsFile $genes_annotations_file\n\n";
my $commandToLoad = "Rscript ../R/explorediffTFresults.R --diffTFResultsVec $diffTF_results_vec --outputFolderVec $output_folder_vec --genesAnnotationsFile $genes_annotations_file";
system($commandToLoad);

