###############
# This script uses the bioconductor package DiffBind to perform differential 
# binding analysis with ChIP-Seq or ATAC-Seq data. It handles only two conditions 
# and the contrasts are built on these two.
# conf file should contain: csv_file;elongation_size;output_folder;scaling_factors;min_overlap;run_parallel;analysis_method_vec;p_value;min_qual_threshold;verbose;bp_around_summit;nb_reads_minThres;remove_duplicates;scale_control;contrast_columns
# Descostes May 2018, updated October 2018
###############

use strict;
my $file_lineNumber = $ARGV[0];
my $input_file = $ARGV[1];


#Retrieve the parameters
my $file_path = `head -n $file_lineNumber $input_file | tail -n1`;
chomp $file_path;

my @arguments_tab = split(';', $file_path);

if(scalar(@arguments_tab) != 16)
{
    die("Missing arguments for diffbind.pl\n\n File should contain: csv_file;analysis_name;elongation_size;output_folder;scaling_factors;min_overlap;run_parallel;analysis_method_vec;p_value;min_qual_threshold;verbose;bp_around_summit;nb_reads_minThres;remove_duplicates;scale_control;contrast_columns\n\n");
}

my $csv_file = $arguments_tab[0];
my $analysis_name = $arguments_tab[1];
my $elongation_size = $arguments_tab[2];
my $output_folder = $arguments_tab[3];
my $scaling_factors = $arguments_tab[4];
my $min_overlap = $arguments_tab[5];
my $run_parallel = $arguments_tab[6];
my $analysis_method_vec = $arguments_tab[7];
my $p_value = $arguments_tab[8];
my $min_qual_threshold = $arguments_tab[9];
my $verbose = $arguments_tab[10];
my $bp_around_summit = $arguments_tab[11];
my $nb_reads_minThres = $arguments_tab[12];
my $remove_duplicates = $arguments_tab[13];
my $scale_control = $arguments_tab[14];
my $contrast_columns = $arguments_tab[15];





print "This is job number $file_lineNumber \n";

print "Rscript ../R/diffbind.R --csvFile $csv_file --analysisName $analysis_name --elongationSize $elongation_size --outputFolder $output_folder --scalingFactors $scaling_factors --minOverlap $min_overlap --runParallel $run_parallel --analysisMethodVec $analysis_method_vec --pValue $p_value --minQualThreshold $min_qual_threshold --verbose $verbose --bpAroundSummit $bp_around_summit --nbReadsMinThres $nb_reads_minThres --removeDuplicates $remove_duplicates --scaleControl $scale_control --contrastColumns $contrast_columns\n\n";
my $commandToLoad = "Rscript ../R/diffbind.R --csvFile $csv_file --analysisName $analysis_name --elongationSize $elongation_size --outputFolder $output_folder --scalingFactors $scaling_factors --minOverlap $min_overlap --runParallel $run_parallel --analysisMethodVec $analysis_method_vec --pValue $p_value --minQualThreshold $min_qual_threshold --verbose $verbose --bpAroundSummit $bp_around_summit --nbReadsMinThres $nb_reads_minThres --removeDuplicates $remove_duplicates --scaleControl $scale_control --contrastColumns $contrast_columns";
system($commandToLoad);



