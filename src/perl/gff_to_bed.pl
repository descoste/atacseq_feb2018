#!/usr/bin/env perl
#This script converts a gff file to a bed file
# It uses the input file files_bed_X.conf of format: PATH;INPUT;NAMEFILE
#Nicolas Descostes November 2015

use strict;
my $file_lineNumber = $ARGV[0];
my $input_file = $ARGV[1];


#Retrieve the parameters
my $file_path = `head -n $file_lineNumber $input_file | tail -n1`;
chomp $file_path;

my @arguments_tab = split(';', $file_path);

if(scalar(@arguments_tab) != 3)
{
	die("Missing arguments for gff_to_bed.pl\n\n File should contain: PATH;INPUT;NAMEFILE\n\n");
}


my $path_input = $arguments_tab[0];
my $input_file = $arguments_tab[1];
my $name_file = $arguments_tab[2];


print "This is job number $file_lineNumber \n";

print "perl core_gff_to_bed.pl $path_input $input_file $name_file\n\n";

my $commandToLoad = "perl ../perl/core_gff_to_bed.pl $path_input $input_file	$name_file";
system($commandToLoad);












