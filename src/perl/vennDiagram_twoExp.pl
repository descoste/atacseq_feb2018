###############
# This script aims at performing venn diagram of interval overlaps between gff files. The script takes 2 files.
# conf file should contain: GFFFILEVEC;OUTPUTFOLDER;EXPNAMEVEC
# Descostes june 2016
###############

use strict;
my $file_lineNumber = $ARGV[0];
my $input_file = $ARGV[1];


#Retrieve the parameters
my $file_path = `head -n $file_lineNumber $input_file | tail -n1`;
chomp $file_path;

my @arguments_tab = split(';', $file_path);

if(scalar(@arguments_tab) != 3)
{
	die("Missing arguments for vennDiagram_twoExp.pl\n\n File should contain: GFFFILEVEC;OUTPUTFOLDER;EXPNAMEVEC\n\n");
}

my $gff_file_vec = $arguments_tab[0];
my $output_folder = $arguments_tab[1];
my $expname_vec = $arguments_tab[2];

print "This is job number $file_lineNumber \n";

print "Rscript ../R/vennDiagram_twoExp.R --gffFileVec $gff_file_vec --outputFolder $output_folder --expnameVec $expname_vec\n\n";
my $commandToLoad = "Rscript ../R/vennDiagram_twoExp.R --gffFileVec $gff_file_vec --outputFolder $output_folder --expnameVec $expname_vec";
system($commandToLoad);
