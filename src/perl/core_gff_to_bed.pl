############
# This program converts a gff file to a bed file according to the format definitions of UCSC.
# command: perl gffToBed path_input input_file	name_file
#
#	The name file will be displayed to the left of the track in a genome browser. 
##
#
# The gff format should be:
#		seqname - The name of the sequence. Must be a chromosome.
#		source - The program that generated this feature. 
#		feature - The name of this type of feature. Some examples of standard feature types are "CDS", "start_codon", "stop_codon", and "exon". 
#		start - The starting position of the feature in the sequence. The first base is numbered 1. 
#		end - The ending position of the feature (inclusive). 
#		score - A score between 0 and 1000. If the track line useScore attribute is set to 1 for this annotation data set, the score value will determine the level of gray in which this feature is displayed (higher numbers = darker gray). If there is no score value, enter ".".
#		strand - Valid entries include '+', '-', or '.' (for don't know/don't care). 
#		frame - If the feature is a coding exon, frame should be a number between 0-2 that represents the reading frame of the first base. If the feature is not a coding exon, the value should be '.'. 
#		group - All lines with the same group are linked together into a single item. 
#
#
# The bed format should be:
#		chrom - The name of the chromosome (e.g. chr3, chrY, chr2_random) or scaffold (e.g. scaffold10671). 
#		chromStart - The starting position of the feature in the chromosome or scaffold. The first base in a chromosome is numbered 0. 
#		chromEnd - The ending position of the feature in the chromosome or scaffold. The chromEnd base is not included in the display of the feature. For example, the first 100 bases of a chromosome are defined as chromStart=0, chromEnd=100, and span the bases numbered 0-99. 
#		name - Defines the name of the BED line. This label is displayed to the left of the BED line in the Genome Browser window when the track is open to full display mode or directly to the left of the item 
#		score - A score between 0 and 1000. If the track line useScore attribute is set to 1 for this annotation data set, the score value will determine the level of gray in which this feature is displayed (higher numbers = darker gray). This table shows the Genome Browser's translation of BED score values into shades of gray
#		strand - Defines the strand - either '+' or '-'. 
#		thickStart - The starting position at which the feature is drawn thickly (for example, the start codon in gene displays). 
#		thickEnd - The ending position at which the feature is drawn thickly (for example, the stop codon in gene displays). 
#		itemRgb - An RGB value of the form R,G,B (e.g. 255,0,0). If the track line itemRgb attribute is set to "On", this RBG value will determine the display color of the data contained in this BED line. NOTE: It is recommended that a simple color scheme (eight colors or less) be used with this attribute to avoid overwhelming the color resources of the Genome Browser and your Internet browser
#		blockCount - The number of blocks (exons) in the BED line. 
#		blockSizes - A comma-separated list of the block sizes. The number of items in this list should correspond to blockCount. 
#		blockStarts - A comma-separated list of block starts. All of the blockStart positions should be calculated relative to chromStart. The number of items in this list should correspond to blockCount.
############

#!/usr/bin/perl
use strict;
use Benchmark;


my $path = $ARGV[0];
my $input_file = $ARGV[1];
my $name_file = $ARGV[2];
my @outputFileTab = split(/\./,$input_file);
my $outputFile = $outputFileTab[0].".bed";


if(!defined($path) || !defined($input_file) || !defined($name_file))
{
	die("\n missing arguments, command: perl gffToBed path_input input_file name_file\n\n");
}


print "writting to $path$outputFile";


open(INPUT, "<$path$input_file") or die("impossible to open $path$input_file for reading: $!\n\n\n");
open(OUTPUT, ">$path$outputFile") or die("impossible to open $path$outputFile for writting: $!\n\n\n");

while(defined(my $line = <INPUT>))
{
		my @informationGFF = split(/\t/,$line);
		my $seqname = $informationGFF[0];
		my $source = $informationGFF[1];
		my $feature = $informationGFF[2];
		my $start = $informationGFF[3];
		my $end = $informationGFF[4];
		my $score = $informationGFF[5];
		my $strand = $informationGFF[6];
		my $frame = $informationGFF[7];
		my $group = $informationGFF[8];
		
		# check if the format is valid
		
		if(!defined($seqname) || !defined($source) || !defined($start) || !defined($end) || !defined($score) || !defined($strand) || !defined($frame) || !defined($group))
		{
			die("\n wrong format for your input file, the format should be:\n\t seqname - The name of the sequence. Must be a chromosome.
																		   \n\t source - The program that generated this feature.
																		   \n\t feature - The name of this type of feature. Some examples of standard feature types are 'CDS', 'start_codon', 'stop_codon', and 'exon'
																		   \n\t start - The starting position of the feature in the sequence. The first base is numbered 1.
																		   \n\t end - The ending position of the feature (inclusive).
																		   \n\t score - A score between 0 and 1000. If the track line useScore attribute is set to 1 for this annotation data set, the score value will determine the level of gray in which this feature is displayed (higher numbers = darker gray). If there is no score value, enter ".".
																		   \n\t strand - Valid entries include '+', '-', or '.' (for don't know/don't care).
																		   \n\t frame - If the feature is a coding exon, frame should be a number between 0-2 that represents the reading frame of the first base. If the feature is not a coding exon, the value should be '.'.
																		   \n\t group - All lines with the same group are linked together into a single item.\n\n\n\n");
		}
		
		#writting the lines in bed format
		
		print(OUTPUT "$seqname\t$start\t$end\t$feature\t0\t$strand\t$start\t$end\t0\t0\t0\t0\n");
}

close(INPUT);
close(OUTPUT);