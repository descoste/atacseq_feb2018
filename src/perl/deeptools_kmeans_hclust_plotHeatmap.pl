#!/usr/bin/env perl
# This script performs kmeans or hierarchical clustering with deeptools.
# It uses the input file files_X.conf of format: see params below
#Nicolas Descostes nov 2018

use strict;
use File::Basename;
use File::Path qw( make_path );

my $file_lineNumber = $ARGV[0];
my $input_file = $ARGV[1];

#Retrieve the parameters
my $file_path = `head -n $file_lineNumber $input_file | tail -n1`;
chomp $file_path;

my @arguments_tab = split(';', $file_path);

if(scalar(@arguments_tab) != 20)
{
	die("Missing arguments for deeptools_kmeans_hclust_plotHeatmap.pl: see params\n\n");
}


my $matrix_file = $arguments_tab[0];
my $outfile_pathname = $arguments_tab[1]; # extensions can be "png", "eps", "pdf" and "svg"
my $outfile_sortedregions_bed = $arguments_tab[2]; 
my $outfile_sortedmatrix = $arguments_tab[3];
my $nbofgroups = $arguments_tab[4];
my $sorting_method= $arguments_tab[5]; #{descend,ascend,no,keep}
my $sort_using = $arguments_tab[6]; # {mean,median,max,min,sum,region_length}                      
my $sort_using_samples= $arguments_tab[7]; # --sortUsing, no value uses all samples, example: --sortUsingSamples 1 3 (default:None)
my $average_type_summary_plot = $arguments_tab[8]; # {mean,median,min,max,std,sum}(default: mean)
my $col_vec = $arguments_tab[9]; # If more than one heatmap is being plotted the color of each heatmap can be enter individually (e.g. `--colorMap Reds Blues`). (default:['RdYlBu'])
my $alpha = $arguments_tab[10];        # Transparency) The default is 1.0 and values must be between 0 and 1. (default: 1.0)
my $boxaroundheatmaps = $arguments_tab[11]; # (default: yes)
my $xaxislabel = $arguments_tab[12];
my $refpointlabels = $arguments_tab[13];
my $label_rotation = $arguments_tab[14]; # (default: 0.0)
my $samples_label = $arguments_tab[15];
my $plot_title = $arguments_tab[16];
my $yaxis_label = $arguments_tab[17];
my $legend_location = $arguments_tab[18]; # {best,upper-right,upper-left,upper-center,lower-left,lower-right,lower-center,center,center-left,center-right,none}
my $clustering_method = $arguments_tab[19];  


my $filenamePath = dirname($outfile_pathname);

if ( !-d $filenamePath ) {
    make_path $filenamePath or die "Failed to create path: $filenamePath";
}


if($clustering_method == "kmeans"){	
	
	print "plotHeatmap --matrixFile $matrix_file --outFileName $outfile_pathname --outFileSortedRegions $outfile_sortedregions_bed --outFileNameMatrix $outfile_sortedmatrix --kmeans $nbofgroups --sortRegions $sorting_method --sortUsing $sort_using --sortUsingSamples $sort_using_samples --averageTypeSummaryPlot $average_type_summary_plot --colorMap $col_vec --alpha $alpha --boxAroundHeatmaps $boxaroundheatmaps --xAxisLabel $xaxislabel --refPointLabel $refpointlabels --labelRotation $label_rotation --samplesLabel $samples_label --plotTitle $plot_title --yAxisLabel $yaxis_label --legendLocation $legend_location --verbose\n\n";
	my $commandToLoad = "plotHeatmap --matrixFile $matrix_file --outFileName $outfile_pathname --outFileSortedRegions $outfile_sortedregions_bed --outFileNameMatrix $outfile_sortedmatrix --kmeans $nbofgroups --sortRegions $sorting_method --sortUsing $sort_using --sortUsingSamples $sort_using_samples --averageTypeSummaryPlot $average_type_summary_plot --colorMap $col_vec --alpha $alpha --boxAroundHeatmaps $boxaroundheatmaps --xAxisLabel $xaxislabel --refPointLabel $refpointlabels --labelRotation $label_rotation --samplesLabel $samples_label --plotTitle $plot_title --yAxisLabel $yaxis_label --legendLocation $legend_location --verbose";
	system($commandToLoad);
	
}elsif($clustering_method == "hclust"){
	
	print "plotHeatmap --matrixFile $matrix_file --outFileName $outfile_pathname --outFileSortedRegions $outfile_sortedregions_bed --outFileNameMatrix $outfile_sortedmatrix --kmeans $nbofgroups --hclust $nbofgroups --sortRegions $sorting_method --sortUsing $sort_using --sortUsingSamples $sort_using_samples --averageTypeSummaryPlot $average_type_summary_plot --colorMap $col_vec --alpha $alpha --boxAroundHeatmaps $boxaroundheatmaps --xAxisLabel $xaxislabel --refPointLabel $refpointlabels --labelRotation $label_rotation --samplesLabel $samples_label --plotTitle $plot_title --yAxisLabel $yaxis_label --legendLocation $legend_location --verbose\n\n";
	my $commandToLoad = "plotHeatmap --matrixFile $matrix_file --outFileName $outfile_pathname --outFileSortedRegions $outfile_sortedregions_bed --outFileNameMatrix $outfile_sortedmatrix --kmeans $nbofgroups --hclust $nbofgroups --sortRegions $sorting_method --sortUsing $sort_using --sortUsingSamples $sort_using_samples --averageTypeSummaryPlot $average_type_summary_plot --colorMap $col_vec --alpha $alpha --boxAroundHeatmaps $boxaroundheatmaps --xAxisLabel $xaxislabel --refPointLabel $refpointlabels --labelRotation $label_rotation --samplesLabel $samples_label --plotTitle $plot_title --yAxisLabel $yaxis_label --legendLocation $legend_location --verbose";
	system($commandToLoad);
	
}else{
	die("The clustering method should be kmeans or hclust")
}

  


  
  