#!/usr/bin/env perl
#This script converts the output of macs2 to a gff file
# It uses the input file X.conf of format: FILETOCONVERT
#Nicolas Descostes ap 2018

use strict;
my $file_lineNumber = $ARGV[0];
my $input_file = $ARGV[1];


#Retrieve the parameters
my $file_param = `head -n $file_lineNumber $input_file | tail -n1`;
chomp $file_param;
my @param_tab = split(';', $file_param);

if(scalar(@param_tab) != 1)
{
    die("Missing arguments for peaks_to_gff.pl\n\n\t input file conf should have FILETOCONVERT\n\n");   
}

my $file_vec = $param_tab[0];

print "This is job number $file_lineNumber \n";

print "Rscript ../R/peaks_to_GFF.R --fileVec $file_vec\n";

my $commandToLoad = "Rscript ../R/peaks_to_GFF.R --fileVec $file_vec";
system($commandToLoad);



