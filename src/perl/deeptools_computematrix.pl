#!/usr/bin/env perl
# This script enables to create a matrix with deeptools
# .conf file should contain: see params in script
# Descostes April 2018

use strict;
use File::Basename;
use File::Path qw( make_path );


my $file_lineNumber = $ARGV[0];
my $nb_cpu = $ARGV[1];
my $input_file = $ARGV[2];


#Retrieve the parameters
my $file_path = `head -n $file_lineNumber $input_file | tail -n1`;
chomp $file_path;

my @arguments_tab = split(';', $file_path);

if(scalar(@arguments_tab) != 15)
{
    die("Missing arguments for deeptools_computeMatrix.pl\n\n File should contain: see params in script\n\n");
}

my $bigwigs_vector = $arguments_tab[0];
my $bed_file = $arguments_tab[1];
my $output_gziped_matrix_path = $arguments_tab[2];
my $output_matrix_table_path = $arguments_tab[3];
my $output_sorted_bed_path = $arguments_tab[4];
my $reference_point = $arguments_tab[5]; # {TSS,TES,center}
my $length_before = $arguments_tab[6];
my $length_after = $arguments_tab[7];
my $binsize = $arguments_tab[8];
my $sorting_method = $arguments_tab[9]; #{descend,ascend,no,keep}
my $sort_using = $arguments_tab[10]; #{mean,median,max,min,sum,region_length}
my $sort_using_samples = $arguments_tab[11]; # no value uses all samples, example: --sortUsingSamples 1 3 (default: None)
my $binning_method = $arguments_tab[12]; # {mean,median,min,max,std,sum}
#my $missingDataAsZero = $arguments_tab[13]; #(default: False)
#my $minthreshold = $arguments_tab[14]; #(default: None)
#my $maxthreshold = $arguments_tab[15]; #(default: None)
#my $blacklist_bed_file = $arguments_tab[13]; #(default: None)
my $samples_label_vec = $arguments_tab[13];
my $scaling_number = $arguments_tab[14]; # 1 = no scaling

my $filenamePath = dirname($output_gziped_matrix_path);

if ( !-d $filenamePath ) {
    make_path $filenamePath or die "Failed to create path: $filenamePath";
}

print "This is job number $file_lineNumber \n";
	
print "computeMatrix reference-point --scoreFileName $bigwigs_vector --regionsFileName $bed_file --outFileName $output_gziped_matrix_path --outFileNameMatrix $output_matrix_table_path --outFileSortedRegions $output_sorted_bed_path --referencePoint $reference_point --beforeRegionStartLength $length_before --afterRegionStartLength $length_after --binSize $binsize --sortRegions $sorting_method --sortUsing $sort_using --sortUsingSamples $sort_using_samples --averageTypeBins $binning_method --samplesLabel $samples_label_vec --scale $scaling_number --numberOfProcessors $nb_cpu";
my $commandToLoad = "computeMatrix reference-point --scoreFileName $bigwigs_vector --regionsFileName $bed_file --outFileName $output_gziped_matrix_path --outFileNameMatrix $output_matrix_table_path --outFileSortedRegions $output_sorted_bed_path --referencePoint $reference_point --beforeRegionStartLength $length_before --afterRegionStartLength $length_after --binSize $binsize --sortRegions $sorting_method --sortUsing $sort_using --sortUsingSamples $sort_using_samples --averageTypeBins $binning_method --samplesLabel $samples_label_vec --scale $scaling_number --numberOfProcessors $nb_cpu";
system($commandToLoad);

  

