###############
# This script aims at performing pie chart of interval overlaps with gff files.
# conf file should contain: BEDFILETOMAP;ANALYSISNAMEVEC;GFFFILEVEC;GFFNAMEVEC;REMAININGCATEGORY;OUTPUTFOLDERVEC
# Descostes March 2016 - update nov 2018
###############

use strict;
my $file_lineNumber = $ARGV[0];
my $input_file = $ARGV[1];


#Retrieve the parameters
my $file_path = `head -n $file_lineNumber $input_file | tail -n1`;
chomp $file_path;

my @arguments_tab = split(';', $file_path);

if(scalar(@arguments_tab) != 6)
{
	die("Missing arguments for overlap_pieChart.pl\n\n File should contain: BEDFILETOMAP;ANALYSISNAMEVEC;GFFFILEVEC;GFFNAMEVEC;REMAININGCATEGORY;OUTPUTFOLDERVEC\n\n");
}


my $bed_file_tomap  = $arguments_tab[0];
my $analysis_name_vec  = $arguments_tab[1];
my $gff_file_vec  = $arguments_tab[2];
my $gff_name_vec  = $arguments_tab[3];
my $remaining_category  = $arguments_tab[4];
my $output_folder_vec  = $arguments_tab[5];

print "This is job number $file_lineNumber \n";

print "Rscript ../R/piechart.R --bedFileToMapVec $bed_file_tomap --analysisNameVec $analysis_name_vec --gffFileVec $gff_file_vec --gffNameVec $gff_name_vec --remainingCategory $remaining_category --outputFolderVec  $output_folder_vec\n\n";

my $commandToLoad = "Rscript ../R/piechart.R --bedFileToMapVec $bed_file_tomap --analysisNameVec $analysis_name_vec --gffFileVec $gff_file_vec --gffNameVec $gff_name_vec --remainingCategory $remaining_category --outputFolderVec  $output_folder_vec";
system($commandToLoad);




