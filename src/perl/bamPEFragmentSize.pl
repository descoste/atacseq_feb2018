#!/usr/bin/env perl
#This script performs statistics of alignment on a bam file
# It uses the input file files_bam_X.conf of format: 
#Nicolas Descostes July 2015

use strict;
use File::Basename;

my $file_lineNumber = $ARGV[0];
my $nbCPU = $ARGV[1];
my $input_file = $ARGV[2];

#Retrieve the parameters
my $file_path = `head -n $file_lineNumber $input_file | tail -n1`;
chomp $file_path;

my @arguments_tab = split(';', $file_path);

if(scalar(@arguments_tab) != 6)
{
	die("Missing arguments for bamPEFragmentSize.pl\n\n File should contain: bamFiles;histogramFile;format;samplelabel;plotTitle;tableFile\n\n");
}


my $bamFiles = $arguments_tab[0];
my $histogramFile = $arguments_tab[1];
my $format = $arguments_tab[2];
my $samplelabel = $arguments_tab[3];
my $plotTitle = $arguments_tab[4];
my $tableFile = $arguments_tab[5];

mkdir dirname($tableFile);

print "This is job number $file_lineNumber\n";

print "bamPEFragmentSize --bamfiles $bamFiles --histogram $histogramFile --plotFileFormat $format --numberOfProcessors $nbCPU --samplesLabel $samplelabel --plotTitle $plotTitle --maxFragmentLength 0 --binSize 1000 --distanceBetweenBins 1000000 --table $tableFile --verbose\n\n";

my $commandToLoad = "bamPEFragmentSize --bamfiles $bamFiles --histogram $histogramFile --plotFileFormat $format --numberOfProcessors $nbCPU --samplesLabel $samplelabel --plotTitle $plotTitle --maxFragmentLength 0 --binSize 1000 --distanceBetweenBins 1000000 --table $tableFile --verbose";
system($commandToLoad);

