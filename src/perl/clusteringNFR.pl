#!/usr/bin/env perl
# This script performs heatmap of kmean clustering of chip-seq data using annotations.
# It uses the input file files_X.conf of format: BIGWIG;BIGWIGNAMEVEC;GFFVEC;ORGANISM;GENOMEVERSION;BINSIZE;PROGILELENGTHBEFORE;PROFILELENGTHAFTER;TYPEVALUE;OUTPUTFOLDER;NBOFGROUPS;INCLUDEEXPVEC;clustering_method;sort_rows;autoscale;zmin;zmax with optional color vec
#Nicolas Descostes feb 2017

use strict;
my $file_lineNumber = $ARGV[0];
my $input_file = $ARGV[1];

#Retrieve the parameters
my $file_path = `head -n $file_lineNumber $input_file | tail -n1`;
chomp $file_path;

my @arguments_tab = split(';', $file_path);

if(scalar(@arguments_tab) != 17 && scalar(@arguments_tab) != 18)
{
	die("Missing arguments for multiple_techniques.pl: BIGWIG;BIGWIGNAMEVEC;GFFVEC;ORGANISM;GENOMEVERSION;BINSIZE;PROGILELENGTHBEFORE;PROFILELENGTHAFTER;TYPEVALUE;OUTPUTFOLDER;NBOFGROUPS;INCLUDEEXPVEC;clustering_method;sort_rows;autoscale;zmin;zmax with optional color vec\n\n");
}


my $bigwig_vec = $arguments_tab[0];
my $bigwig_name_vec = $arguments_tab[1];
my $gff_vec = $arguments_tab[2];
my $organism = $arguments_tab[3];
my $genome_version = $arguments_tab[4];
my $bin_size = $arguments_tab[5];
my $profile_length_before = $arguments_tab[6];
my $profile_length_after = $arguments_tab[7];
my $type_value = $arguments_tab[8];
my $output_folder = $arguments_tab[9];
my $nb_of_groups = $arguments_tab[10];
my $include_exp_vec = $arguments_tab[11];
my $clustering_method = $arguments_tab[12];
my $sort_rows = $arguments_tab[13];
my $auto_scale = $arguments_tab[14];
my $zmin = $arguments_tab[15];
my $zmax = $arguments_tab[16];


print "This is job number $file_lineNumber \n";

if(scalar(@arguments_tab) == 17)
{
	print "Rscript ../R/clustering_NFR.R --bigwigVec $bigwig_vec --bigwigNameVec $bigwig_name_vec --gffVec $gff_vec --organism $organism --genomeVersion $genome_version --binSize $bin_size --profileLengthBefore $profile_length_before --profileLengthAfter $profile_length_after --typeValue $type_value --outputFolder $output_folder --nbOfGroups $nb_of_groups --includeExpVec $include_exp_vec --clusteringMethod $clustering_method --sortRows $sort_rows --autoScale $auto_scale --zmin $zmin --zmax $zmax\n\n";

	my $commandToLoad = "Rscript ../R/clustering_NFR.R --bigwigVec $bigwig_vec --bigwigNameVec $bigwig_name_vec --gffVec $gff_vec --organism $organism --genomeVersion $genome_version --binSize $bin_size --profileLengthBefore $profile_length_before --profileLengthAfter $profile_length_after --typeValue $type_value --outputFolder $output_folder --nbOfGroups $nb_of_groups --includeExpVec $include_exp_vec --clusteringMethod $clustering_method --sortRows $sort_rows --autoScale $auto_scale --zmin $zmin --zmax $zmax";	
	system($commandToLoad);
	
}else{
	
	# Define vector of colors
	my $col_value = $arguments_tab[17];
	
	print "Rscript ../R/clustering_NFR.R --bigwigVec $bigwig_vec --bigwigNameVec $bigwig_name_vec --gffVec $gff_vec --organism $organism --genomeVersion $genome_version --binSize $bin_size --profileLengthBefore $profile_length_before --profileLengthAfter $profile_length_after --typeValue $type_value --outputFolder $output_folder --nbOfGroups $nb_of_groups --includeExpVec $include_exp_vec --clusteringMethod $clustering_method --sortRows $sort_rows --autoScale $auto_scale --zmin $zmin --zmax $zmax --colValue $col_value\n\n";
	my $commandToLoad = "Rscript ../R/clustering_NFR.R --bigwigVec $bigwig_vec --bigwigNameVec $bigwig_name_vec --gffVec $gff_vec --organism $organism --genomeVersion $genome_version --binSize $bin_size --profileLengthBefore $profile_length_before --profileLengthAfter $profile_length_after --typeValue $type_value --outputFolder $output_folder --nbOfGroups $nb_of_groups --includeExpVec $include_exp_vec --clusteringMethod $clustering_method --sortRows $sort_rows --autoScale $auto_scale --zmin $zmin --zmax $zmax --colValue $col_value";
	system($commandToLoad);
}





