##############
# This script output a bed of binding locations for each TF with a positive or
# negative LFC with adj-pval < 0.05.
# The considered TFs in each condition are:
# Tal1, Runx1, Gata2, Erg, and fli1 (Lyl1 will be with Hocomoco v11)
# Descostes Feb 2020
##############



#############
## PARAMS
#############


tf_summary_table_vec <- list("i3untvsdox" = c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200203_i3untvsdox/TF-SPECIFIC/TAL1.A/extension100/i3untvsdox.all.TAL1.A.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200203_i3untvsdox/TF-SPECIFIC/RUNX1/extension100/i3untvsdox.all.RUNX1.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200203_i3untvsdox/TF-SPECIFIC/GATA2/extension100/i3untvsdox.all.GATA2.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200203_i3untvsdox/TF-SPECIFIC/ERG/extension100/i3untvsdox.all.ERG.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200203_i3untvsdox/TF-SPECIFIC/FLI1/extension100/i3untvsdox.all.FLI1.output.tsv"),
		
		"i5doxEndovsi8dox" = c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxEndovsi8dox/TF-SPECIFIC/TAL1.A/extension100/i5doxEndovsi8dox.all.TAL1.A.output.tsv", 
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxEndovsi8dox/TF-SPECIFIC/RUNX1/extension100/i5doxEndovsi8dox.all.RUNX1.output.tsv", 
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxEndovsi8dox/TF-SPECIFIC/GATA2/extension100/i5doxEndovsi8dox.all.GATA2.output.tsv", 
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxEndovsi8dox/TF-SPECIFIC/ERG/extension100/i5doxEndovsi8dox.all.ERG.output.tsv", 
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxEndovsi8dox/TF-SPECIFIC/FLI1/extension100/i5doxEndovsi8dox.all.FLI1.output.tsv"),
		
		"i5doxPreHSCvsi5doxEndo" = c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxPreHSCvsi5doxEndo/TF-SPECIFIC/TAL1.A/extension100/i5doxPreHSCvsi5doxEndo.all.TAL1.A.output.tsv", 
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxPreHSCvsi5doxEndo/TF-SPECIFIC/RUNX1/extension100/i5doxPreHSCvsi5doxEndo.all.RUNX1.output.tsv", 
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxPreHSCvsi5doxEndo/TF-SPECIFIC/GATA2/extension100/i5doxPreHSCvsi5doxEndo.all.GATA2.output.tsv", 
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxPreHSCvsi5doxEndo/TF-SPECIFIC/ERG/extension100/i5doxPreHSCvsi5doxEndo.all.ERG.output.tsv", 
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxPreHSCvsi5doxEndo/TF-SPECIFIC/FLI1/extension100/i5doxPreHSCvsi5doxEndo.all.FLI1.output.tsv"),
		
		"i5doxPreHSCvsi8dox" = c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxPreHSCvsi8dox/TF-SPECIFIC/ERG/extension100/i5doxPreHSCvsi8dox.all.ERG.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxPreHSCvsi8dox/TF-SPECIFIC/FLI1/extension100/i5doxPreHSCvsi8dox.all.FLI1.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxPreHSCvsi8dox/TF-SPECIFIC/GATA2/extension100/i5doxPreHSCvsi8dox.all.GATA2.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxPreHSCvsi8dox/TF-SPECIFIC/RUNX1/extension100/i5doxPreHSCvsi8dox.all.RUNX1.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5doxPreHSCvsi8dox/TF-SPECIFIC/TAL1.A/extension100/i5doxPreHSCvsi8dox.all.TAL1.A.output.tsv"),
		
		"i5untvsDoxEndo" = c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5untvsDoxEndo/TF-SPECIFIC/ERG/extension100/i5untvsdoxEndo.all.ERG.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5untvsDoxEndo/TF-SPECIFIC/FLI1/extension100/i5untvsdoxEndo.all.FLI1.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5untvsDoxEndo/TF-SPECIFIC/GATA2/extension100/i5untvsdoxEndo.all.GATA2.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5untvsDoxEndo/TF-SPECIFIC/RUNX1/extension100/i5untvsdoxEndo.all.RUNX1.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5untvsDoxEndo/TF-SPECIFIC/TAL1.A/extension100/i5untvsdoxEndo.all.TAL1.A.output.tsv"),
		
		"i5untvsDoxPreHSC" = c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5untvsDoxPreHSC/TF-SPECIFIC/ERG/extension100/i5untvsdoxPreHSC.all.ERG.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5untvsDoxPreHSC/TF-SPECIFIC/FLI1/extension100/i5untvsdoxPreHSC.all.FLI1.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5untvsDoxPreHSC/TF-SPECIFIC/GATA2/extension100/i5untvsdoxPreHSC.all.GATA2.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5untvsDoxPreHSC/TF-SPECIFIC/RUNX1/extension100/i5untvsdoxPreHSC.all.RUNX1.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i5untvsDoxPreHSC/TF-SPECIFIC/TAL1.A/extension100/i5untvsdoxPreHSC.all.TAL1.A.output.tsv"),
		
		"i8untvsdox" = c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i8untvsDox/TF-SPECIFIC/ERG/extension100/i8untvsdox.all.ERG.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i8untvsDox/TF-SPECIFIC/FLI1/extension100/i8untvsdox.all.FLI1.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i8untvsDox/TF-SPECIFIC/GATA2/extension100/i8untvsdox.all.GATA2.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i8untvsDox/TF-SPECIFIC/RUNX1/extension100/i8untvsdox.all.RUNX1.output.tsv",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/20200206_i8untvsDox/TF-SPECIFIC/TAL1.A/extension100/i8untvsdox.all.TAL1.A.output.tsv"))

TF_name_vec <- c("Tal1", "Runx1", "Gata2", "Erg", "fli1")


output_folder <- "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/version1-6/upset_diagrams/input_files/"



#############
## FUNCTIONS
#############

#tablefi = fi
#idx=idx_neg
#fctype="negative"

write_bed <- function(tablefi, idx, output_folder, fctype, 
		current_condition_name, current_tf){
	
	if(!isTRUE(all.equal(length(idx),0))){
		
		fi_select <- tablefi[idx,]
		to_write <- data.frame(chr=fi_select$chr,
				start=fi_select$MSS,
				end=fi_select$MES,
				name = fi_select$TF,
				score = 0,
				strand = fi_select$strand,
				thickstart = fi_select$MSS,
				thickend=fi_select$MES,
				itemrgb="0,0,0",
				blc=".",
				blsi=".",
				blst=".")
		
		output_file <- paste0(output_folder, 
				current_condition_name, "/",
				current_condition_name, "_",
				current_tf, if(fctype == "negative") "down.bed" else "up.bed")
		
		if(!file.exists(dirname(output_file)))
			dir.create(dirname(output_file), recursive=T)
		
		write.table(to_write, file=output_file, sep="\t",
				quote=F, row.names=F, col.names=F)
		
	}else{
		if(fctype == "negative")
			cat("\t\t No downregulated TFBS found\n")
		else
			cat("\t\t No upregulated TFBS found\n")
	}
}

#############
## MAIN
#############


conditions_vec <- names(tf_summary_table_vec)


#current_condition_path = tf_summary_table_vec[[1]]
#current_condition_name = conditions_vec[1]
#tf_vec= TF_name_vec

invisible(mapply(function(current_condition_path, current_condition_name, tf_vec){
			
			cat("Processing ", current_condition_name, "\n")
			
#			current_path = current_condition_path[1]
#			current_tf = tf_vec[1]
			
			mapply(function(current_path, current_tf){
						
						cat("\t Processing ", current_tf, "\n")
						
						fi <- read.csv(current_path, sep="\t")
						idx_signif <- which(fi$pval_adj < 0.05)
						
						if(!isTRUE(all.equal(length(idx_signif),0))){
							
							fi <- fi[idx_signif,]
							idx_neg <- which(fi$l2FC <= -0.8)
							idx_pos <- which(fi$l2FC >= 0.8)
							write_bed(fi, idx_neg, output_folder, "negative", 
									current_condition_name, current_tf)
							write_bed(fi, idx_neg, output_folder, "positive", 
									current_condition_name, current_tf)						
							
						}else{
							cat("\t\t No signif DE TFBS\n\n")
						}
							
					}, current_condition_path, tf_vec)
			
		}, tf_summary_table_vec, conditions_vec, MoreArgs = list(TF_name_vec)))
