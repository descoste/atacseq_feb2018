#-------------------------------
#-------------------------------
# PREPARING CSV FILES
#

Condition <- list(c("condition1", "condition1", "condition2", "condition2"),
		c("condition1", "condition1", "condition2", "condition2"),
		c("condition1", "condition1", "condition2", "condition2"),
		c("condition1", "condition1", "condition2", "condition2"),
		c("condition1", "condition1", "condition2", "condition2"),
		c("condition1", "condition1", "condition2", "condition2"),
		c("condition1", "condition1", "condition2", "condition2"),
		c("condition1", "condition1", "condition2", "condition2"),
		c("condition1", "condition1", "condition2", "condition2"),
		c("condition1", "condition1", "condition2", "condition2"),
		c("condition1", "condition1", "condition2", "condition2"))

Treatment <- list(c("none", "none", "dox", "dox"),
		c("none", "none", "none", "none"),
		c("none", "none", "dox", "dox"),
		c("none", "none", "dox", "dox"),
		c("none", "none", "dox", "dox"),
		c("none", "none", "none", "none"),
		c("none", "none", "dox", "dox"),
		c("none", "none", "dox", "dox"),
		c("none", "none", "dox", "dox"),
		c("none", "none", "none", "none"),
		c("none", "none", "dox", "dox"))
Replicate <- list(c(1,2,1,2),
		c(1,2,1,2),
		c(1,2,1,2),
		c(1,2,1,2),
		c(1,2,1,2),
		c(1,2,1,2),
		c(1,2,1,2),
		c(1,2,1,2),
		c(1,2,1,2),
		c(1,2,1,2),
		c(1,2,1,2))
PeakCaller <- list(c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"),
		c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"),
		c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"),
		c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"))
PeakFormat <- list(c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"),
		c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"),
		c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"),
		c("bed", "bed", "bed", "bed"), c("bed", "bed", "bed", "bed"))

## Mouse
Tissue <- list(c("untVSM", "untVSM", "doxVSM", "doxVSM"),
		c("parentVSM", "parentVSM", "untVSM", "untVSM"),
		c("parentVSM", "parentVSM", "doxVSM", "doxVSM"),
		c("untVSM", "untVSM", "doxPreHSC", "doxPreHSC"),
		c("untVSM", "untVSM", "doxEndo", "doxEndo"),
		c("parentVSM", "parentVSM","untVSM", "untVSM"),
		c("parentVSM", "parentVSM","doxPreHSC", "doxPreHSC"),
		c("parentVSM", "parentVSM","doxEndo", "doxEndo"),
		c("untVSM", "untVSM", "doxPreHSC", "doxPreHSC"),
		c("parentVSM", "parentVSM","untVSM", "untVSM"),
		c("parentVSM", "parentVSM","doxPreHSC", "doxPreHSC"))


# HDGF2
SampleID <- list(c("i3untVSM-1", "i3untVSM-2", "i3doxVSM-1", "i3doxVSM-2"),
		c("i3parentVSM-1", "i3parentVSM-2", "i3untVSM-1", "i3untVSM-2"),
		c("i3parentVSM-1", "i3parentVSM-2", "i3doxVSM-1", "i3doxVSM-2"),
		c("i5untVSM-1", "i5untVSM-2", "i5doxPreHSC-1", "i5doxPreHSC-2"),
		c("i5untVSM-1", "i5untVSM-2", "i5doxEndo-1", "i5doxEndo-2"),
		c("i5ParentVSM-1", "i5ParentVSM-2","i5untVSM-1", "i5untVSM-2"),
		c("i5ParentVSM-1", "i5ParentVSM-2","i5doxPreHSC-1", "i5doxPreHSC-2"),
		c("i5ParentVSM-1", "i5ParentVSM-2","i5doxEndo-1", "i5doxEndo-2"),
		c("i8untVSM-1", "i8untVSM-2", "i8doxPreHSC-1", "i8doxPreHSC-2"),
		c("i8ParentVSM-1", "i8ParentVSM-2","i8untVSM-1", "i8untVSM-2"),
		c("i8ParentVSM-1", "i8ParentVSM-2","i8doxPreHSC-1", "i8doxPreHSC-2"))

bamReads <- list(c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3untVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3untVSM-2.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3doxVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3doxVSM-2.noGCBias.final.bam"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3parentVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3parentVSM-2.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3untVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3untVSM-2.noGCBias.final.bam"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3parentVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3parentVSM-2.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3doxVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i3doxVSM-2.noGCBias.final.bam"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5untVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5untVSM-2.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5doxPreHSC-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5doxPreHSC-2.noGCBias.final.bam"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5untVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5untVSM-2.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5doxEndo-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5doxEndo-2.noGCBias.final.bam"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5parentVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5parentVSM-2.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5untVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5untVSM-2.noGCBias.final.bam"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5parentVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5parentVSM-2.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5doxPreHSC-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5doxPreHSC-2.noGCBias.final.bam"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5parentVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5parentVSM-2.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5doxEndo-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i5doxEndo-2.noGCBias.final.bam"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8untVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8untVSM-2.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8doxPreHSC-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8doxPreHSC-2.noGCBias.final.bam"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8parentVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8parentVSM-2.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8untVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8untVSM-2.noGCBias.final.bam"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8parentVSM-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8parentVSM-2.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8doxPreHSC-1.noGCBias.final.bam", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/sequencing/bam/i8doxPreHSC-2.noGCBias.final.bam"))


Factor <- list(c("untVSM", "untVSM", "doxVSM", "doxVSM"),
		c("parentVSM", "parentVSM", "untVSM", "untVSM"),
		c("parentVSM", "parentVSM", "doxVSM", "doxVSM"),
		c("untVSM", "untVSM", "doxPreHSC", "doxPreHSC"),
		c("untVSM", "untVSM", "doxEndo", "doxEndo"),
		c("parentVSM", "parentVSM","untVSM", "untVSM"),
		c("parentVSM", "parentVSM","doxPreHSC", "doxPreHSC"),
		c("parentVSM", "parentVSM","doxEndo", "doxEndo"),
		c("untVSM", "untVSM", "doxPreHSC", "doxPreHSC"),
		c("parentVSM", "parentVSM","untVSM", "untVSM"),
		c("parentVSM", "parentVSM","doxPreHSC", "doxPreHSC"))




Peaks <- list(c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3untVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3untVSM-2_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3doxVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3doxVSM-2_noGCBias.bed"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3parentVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3parentVSM-2_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3untVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3untVSM-2_noGCBias.bed"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3parentVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3parentVSM-2_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3doxVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i3doxVSM-2_noGCBias.bed"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5untVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5untVSM-2_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5doxPreHSC-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5doxPreHSC-2_noGCBias.bed"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5untVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5untVSM-2_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5doxEndo-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5doxEndo-2_noGCBias.bed"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5parentVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5parentVSM-2_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5untVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5untVSM-2_noGCBias.bed"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5parentVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5parentVSM-2_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5doxPreHSC-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5doxPreHSC-2_noGCBias.bed"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5parentVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5parentVSM-2_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5doxEndo-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i5doxEndo-2_noGCBias.bed"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8untVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8untVSM-2_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8doxPreHSC-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8doxPreHSC-2_noGCBias.bed"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8parentVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8parentVSM-2_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8untVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8untVSM-2_noGCBias.bed"),
		c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8parentVSM-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8parentVSM-2_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8doxPreHSC-1_noGCBias.bed", "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/peak_calling/with_macs2/0.04/no_model_broad/i8doxPreHSC-2_noGCBias.bed"))


outputFile_vec <- c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i3unt_vs_i3dox.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i3parent_vs_i3unt.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i3parent_vs_i3dox.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i5unt_vs_i5doxPreHSC.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i5unt_vs_i5doxEndo.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i5parent_vs_i5unt.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i5parent_vs_i5doxPreHSC.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i5parent_vs_i5doxEndo.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i8unt_vs_i8dox.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i8parent_vs_i8unt.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i8parent_vs_i8dox.csv")
		
for (i in 1:length(Peaks)) {
	to_write <- cbind(SampleID = SampleID[[i]], Tissue = Tissue[[i]], Factor = Factor[[i]], Condition = Condition[[i]], 
			Treatment = Treatment[[i]], Replicate = Replicate[[i]], 
			bamReads = bamReads[[i]], Peaks = Peaks[[i]], PeakCaller = PeakCaller[[i]], PeakFormat = PeakFormat[[i]])
	write.csv(to_write, file= outputFile_vec[i], quote=FALSE, row.names = FALSE)
	
}		
		


#-------------------------------
#-------------------------------
# PREPARING DIFFBIND CONF
#




csv_file <- c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i3unt_vs_i3dox.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i3parent_vs_i3unt.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i3parent_vs_i3dox.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i5unt_vs_i5doxPreHSC.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i5unt_vs_i5doxEndo.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i5parent_vs_i5unt.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i5parent_vs_i5doxPreHSC.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i5parent_vs_i5doxEndo.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i8unt_vs_i8dox.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i8parent_vs_i8unt.csv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/csv_files/i8parent_vs_i8dox.csv")

analysis_name <- c("i3unt_vs_i3dox",
		"i3parent_vs_i3unt",
		"i3parent_vs_i3dox",
		"i5unt_vs_i5doxPreHSC",
		"i5unt_vs_i5doxEndo",
		"i5parent_vs_i5unt",
		"i5parent_vs_i5doxPreHSC",
		"i5parent_vs_i5doxEndo",
		"i8unt_vs_i8dox",
		"i8parent_vs_i8unt",
		"i8parent_vs_i8dox")


elongation_size <- list(paste(128, 138, 107, 131, sep=" "),
		paste(103, 105, 128, 138, sep=" "),
		paste(103, 105, 107, 131, sep=" "),
		paste(103, 113, 113, 108, sep=" "),
		paste(103, 113, 100, 116, sep=" "),
		paste(116, 143, 103, 113, sep=" "),
		paste(116, 143, 113, 108, sep=" "),
		paste(116, 143, 100, 116, sep=" "),
		paste(116, 131, 97, 126, sep=" "),
		paste(104, 106, 116, 131, sep=" "),
		paste(104, 106, 97, 126, sep=" "))


output_folder <- c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/results/i3unt_vs_i3dox/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/results/i3parent_vs_i3unt/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/results/i3parent_vs_i3dox/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/results/i5unt_vs_i5doxPreHSC/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/results/i5unt_vs_i5doxEndo/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/results/i5parent_vs_i5unt/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/results/i5parent_vs_i5doxPreHSC/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/results/i5parent_vs_i5doxEndo/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/results/i8unt_vs_i8dox/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/results/i8parent_vs_i8unt/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffbind/results/i8parent_vs_i8dox/")


scaling_factors <- "NULL"
min_overlap <- 0
run_parallel <- TRUE
analysis_method_vec <- "DESEQ2" 
p_value <- 0.05
min_qual_threshold <- 15 
verbose <- TRUE
bp_around_summit <- "FALSE" 
nb_reads_minThres <- 0
remove_duplicates <- "FALSE" 
scale_control <- "FALSE" 
contrast_columns <- "DBA_CONDITION" 

to_write <- vector()

for (i in 1:length(output_folder)) {
	
	to_write <- c(to_write, paste(csv_file[i], analysis_name[i], elongation_size[[i]], output_folder[i], scaling_factors, min_overlap,
			run_parallel, analysis_method_vec, p_value, min_qual_threshold, verbose, bp_around_summit,
			nb_reads_minThres, remove_duplicates, scale_control, contrast_columns, sep=";"))
	
}

write(to_write, file="../../conf/20181023_diffbind.conf", ncolumns=1)