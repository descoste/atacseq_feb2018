

bed_file_toMap_vec <- c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/clustering/kmeans_deeptools/unt_vs_dox/results/3_group/kmeans_3group_cluster_3.bed",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/clustering/kmeans_deeptools/unt_vs_dox/results/3_group/kmeans_3group_cluster_2.bed",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/clustering/kmeans_deeptools/unt_vs_dox/results/3_group/kmeans_3group_cluster_1.bed",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/clustering/kmeans_deeptools/unt_vs_dox/results/4_group/kmeans_4group_cluster_1.bed",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/clustering/kmeans_deeptools/unt_vs_dox/results/4_group/kmeans_4group_cluster_3.bed",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/clustering/kmeans_deeptools/unt_vs_dox/results/4_group/kmeans_4group_cluster_2.bed",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/clustering/kmeans_deeptools/unt_vs_dox/results/4_group/kmeans_4group_cluster_4.bed")

analysis_name_vec <- c("kmeans_3group_cluster_3",
		"kmeans_3group_cluster_2",
		"kmeans_3group_cluster_1",
		"kmeans_4group_cluster_1",
		"kmeans_4group_cluster_3",
		"kmeans_4group_cluster_2",
		"kmeans_4group_cluster_4")

gff_file_vec <- paste("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/genome/mm10/20181126_mm10-exons_chromFiltered.gff",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/genome/mm10/20181126_mm10-introns_chromfiltered.gff",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/genome/mm10/20181126_mm10-promoters-upstream1000_chromFiltered.gff", sep=" ")
gff_name_vec <- paste("exons", "introns", "promoters", sep=" ")
remaining_category <- "intergenic"
output_folder_vec <- c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/piechart/kmeans_deeptools/kmeans_3group_cluster_3/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/piechart/kmeans_deeptools/kmeans_3group_cluster_2/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/piechart/kmeans_deeptools/kmeans_3group_cluster_1/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/piechart/kmeans_deeptools/kmeans_4group_cluster_1/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/piechart/kmeans_deeptools/kmeans_4group_cluster_3/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/piechart/kmeans_deeptools/kmeans_4group_cluster_2/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/piechart/kmeans_deeptools/kmeans_4group_cluster_4/")


to_write <- paste(bed_file_toMap_vec, analysis_name_vec, gff_file_vec, gff_name_vec, remaining_category, output_folder_vec, sep=";")

cat("Nb of lines should be ", length(bed_file_toMap_vec))

write(to_write, file="../../conf/20181126_piechart.conf", ncolumns=1)
