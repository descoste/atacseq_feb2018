tsv_files <- c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20181217_i3untvsdox/FINAL_OUTPUT/extension100/i3untvsdox.all.allMotifs.tsv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190102_i5untvsDoxEndo/FINAL_OUTPUT/extension100/i5untvsdoxEndo.all.allMotifs.tsv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190102_i5untvsDoxPreHSC/FINAL_OUTPUT/extension100/i5untvsdoxPreHSC.all.allMotifs.tsv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190102_i8untvsDox/FINAL_OUTPUT/extension100/i8untvsdox.all.allMotifs.tsv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190130_i5doxEndovsi8dox/FINAL_OUTPUT/extension100/i5doxEndovsi8dox.all.allMotifs.tsv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190130_i5doxPreHSCvsi5doxEndo/FINAL_OUTPUT/extension100/i5doxPreHSCvsi5doxEndo.all.allMotifs.tsv",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190130_i5doxPreHSCvsi8dox/FINAL_OUTPUT/extension100/i5doxPreHSCvsi8dox.all.allMotifs.tsv")

output_folder_vec <- c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20181217_i3untvsdox/plot_nico/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190102_i5untvsDoxEndo/plot_nico/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190102_i5untvsDoxPreHSC/plot_nico/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190102_i8untvsDox/plot_nico/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190130_i5doxEndovsi8dox/plot_nico/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190130_i5doxPreHSCvsi5doxEndo/plot_nico/",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/diffTF/20190130_i5doxPreHSCvsi8dox/plot_nico/")

genes_annotations_file <- "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/genome/mm10/20181123_mm10_filtered.gff"


to_write <- paste(tsv_files, output_folder_vec, genes_annotations_file, sep=";")

cat("Nb should be: ", length(tsv_files), "\n")

write(to_write, file="../../conf/20190315_explorediffTFresults.conf", ncolumns=1)