#################
# This script performs the overlap of two exp given as gff and output the individual overlapping peaks.
# Descostes May 2016 - update Nov 2018
#################


library(NGSprofiling);
library("Rargs");
library("GenomicRanges")
library("ChIPpeakAnno");

################
# PARAMETERS
################


#parameters defined from the command line using RIO
paramsDefinition <- list();

#firstly, definition of mandatory parameters that have to be given by the user in the command line

paramsDefinition[["--gffFileVec"]] <- list(variableName="gff_file_vec", numeric=F, mandatory=T, description="Vector containing file path in gff to the exp to compare.");
paramsDefinition[["--outputFolder"]] <- list(variableName="output_folder", numeric=F, mandatory=T, description="single path to the output folder.");
paramsDefinition[["--expnameVec"]] <- list(variableName="expname_vec", numeric=F, mandatory=T, description="Vector containing name of each experiment given as gff files.");

gff_file_vec <- c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/genome/mm10/20181123_mm10_filtered.gff",
				"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/clustering/kmeans_deeptools/unt_vs_dox/results/4_group/kmeans_4group_cluster_4.gff")
output_folder <- "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/overlap/kmeans_deeptools/refseq_vs_4groups_cluster4/twoExp/"
expname_vec <- c("refseq", "4groups_cluster4")


################



##############
# MAIN
##############


# Retreives the parameters
getParams(paramsDefinition);


if(length(gff_file_vec) != 2 || length(expname_vec) != 2)
{
	stop("\n This script takes only two exp as input\n");
}

checkingOutputFolder(output_folder);


cat("Reading gff input and converting to rangedData\n");

gff_GRanges_list <- list();

for(i in 1:length(gff_file_vec)) 
{
	current_gff <- read.table(gff_file_vec[i], stringsAsFactors=F);
	
	#inversion of source and feature
	tmp <- current_gff$V2;
	current_gff$V2 <- current_gff$V3;
	current_gff$V3 <- tmp;
	
	if(length(current_gff$V2) != length(unique(current_gff$V2)))
	{
		current_gff$V2 <- make.unique(as.character(current_gff$V2), sep="-");
	}
	
	gff_GRanges_list[[i]] <- GRanges(seqnames= current_gff[,1], ranges=IRanges(start= current_gff[,4], end = current_gff[,5], names = current_gff[,2]), strand=current_gff[,7])
}

cat("Performing the overlap\n");

result_overlap <- findOverlappingPeaks(gff_GRanges_list[[1]], gff_GRanges_list[[2]], NameOfPeaks1 = expname_vec[1], NameOfPeaks2 = expname_vec[2]);


cat("Writting the overlapping peaks\n");

gff_table_peak1 <- data.frame(seqname=as.character(seqnames(result_overlap$Peaks1withOverlaps)), 
		source="vennDiagram_overlapGFF", 
		feature = names(result_overlap$Peaks1withOverlaps), 
		start=start(result_overlap$Peaks1withOverlaps),
		end=end(result_overlap$Peaks1withOverlaps),
		score=0,
		strand=as.character(strand(result_overlap$Peaks1withOverlaps)),
		frame=".",
		group=".")

gff_table_peak2 <- data.frame(seqname=as.character(seqnames(result_overlap$Peaks2withOverlaps)), 
		source="vennDiagram_overlapGFF", 
		feature = names(result_overlap$Peaks2withOverlaps), 
		start=start(result_overlap$Peaks2withOverlaps),
		end=end(result_overlap$Peaks2withOverlaps),
		score=0,
		strand=as.character(strand(result_overlap$Peaks2withOverlaps)),
		frame=".",
		group=".")

write.table(gff_table_peak1, file=paste(output_folder, expname_vec[1], ".gff",  sep=""), sep="\t", quote=F, row.names=F, col.names=F);
write.table(gff_table_peak2, file=paste(output_folder, expname_vec[2], ".gff", sep=""), sep="\t", quote=F, row.names=F, col.names=F);


