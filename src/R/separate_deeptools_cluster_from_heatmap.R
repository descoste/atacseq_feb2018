#############
## This script aims at separating the intervals by cluster belonging.
## It takes the bed file output by plotHeatmap script.
##  Descostes nov 2018
#############



#############
## PARAMS
#############


bed_vec <- c("/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/clustering/kmeans_deeptools/unt_vs_dox/results/3_group/kmeans_3group.bed",
		"/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/analysis/clustering/kmeans_deeptools/unt_vs_dox/results/4_group/kmeans_4group.bed")


#############
## MAIN
#############

for (i in seq_len(length(bed_vec))) {
	
	fi <- read.table(bed_vec[i], stringsAsFactors = FALSE)
	
	cluster_list <- split(fi, fi[,13])
	
	invisible(lapply(cluster_list, function(clus){
						
						output_file <- 
								paste0(unlist(strsplit(bed_vec[i], ".bed")), 
										"_", unique(clus[,13]), ".bed")
						write.table(clus, file=output_file, sep="\t", 
								quote = FALSE, row.names = FALSE, 
								col.names = FALSE)
					}))
}