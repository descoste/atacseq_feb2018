###########
# This script aims at building the gene translation table for diffTF for 
# hocomoco v11. The model table can be found in
# /g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/softwares/diffTF/src/TF_Gene_TranslationTables/HOCOMOCO_v10
#
# Descostes feb 2020
############

library("clusterProfiler")


#############
## PARAMS
#############


output_file <- "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/softwares/diffTF/src/TF_Gene_TranslationTables/HOCOMOCO_v11/translationTable_mm10.csv"

hocomoco_table <- "/g/lancrin/Projects/Nicolas/ATACSEQ_Feb2018/data/genome/mm10/HOCOMOCOv11_core_annotation_MOUSE_mono.tsv"

database_name <-"org.Mm.eg.db"


#############
## MAIN
#############


## Read hocomoco file
hocfi <- read.csv(hocomoco_table, sep="\t", stringsAsFactors=FALSE)

## Retrieve ENSEMBL	gene ID
ensembl_table <- bitr(hocfi$EntrezGene, fromType="ENTREZID", toType=c("SYMBOL", "ENSEMBL"), OrgDb= database_name)
ensembl_table <- ensembl_table[-which(duplicated(ensembl_table$ENTREZID)),]

## Collecting missing IDs
idx <- match(hocfi$EntrezGene, ensembl_table$ENTREZID)
idx_na <- which(is.na(idx))
if(!isTRUE(all.equal(length(idx_na),0)))
	stop("Some entrezID were not retrieved\n")

## Retrieve HocoID
hocoid_vec <- unlist(lapply(strsplit(hocfi$Model,"_MOUSE"),"[",1))

if(!isTRUE(all.equal(length(hocoid_vec), nrow(ensembl_table))))
	stop("Pb in the script, contact the developper\n")

hocoid_vec <- hocoid_vec[idx]

## Create output table
to_write <- data.frame(SYMBOL=ensembl_table$SYMBOL, ENSEMBL=ensembl_table$ENSEMBL, HOCOID=hocoid_vec)
write.csv(to_write, file = output_file, quote=F, sep="\t", row.names=F)
